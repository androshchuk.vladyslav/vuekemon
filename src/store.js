import Vue from 'vue'
import Vuex from 'vuex'
import { Api } from "@/common/api";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pokemons: [],
    pokemonsList: []
  },
  getters: {
    pokemonsCount: state => {
      return state.pokemonsList.count;
    }
  },
  mutations: {
    setPokemons(state, pokemons) {
      state.pokemons = pokemons;
    },
    setPokemonsList(state, pokemons) {
      state.pokemonsList = pokemons;
    }
  },
  actions: {
    getPokemonsList(context) {
      return Api.get('pokemon').then(({data}) => {
        context.commit('setPokemonsList', data);
      });
    },
    getPokemons(context, payload) {
      let promise = Promise.resolve();

      if (context.state.pokemonsList.length === 0) {
        promise = context.dispatch('getPokemonsList');
      }

      return promise.then( () => {
        let startSlice = payload.offset || 1;
        let toSlice = startSlice + (payload.limit || 1);
        let pokemonsIds = context.state.pokemonsList.results.map(p => p.name).slice(startSlice, toSlice);

        return Api.getMany('pokemon', pokemonsIds).then(axios.spread((...args) => {
          context.commit('setPokemons', args.map(o => o.data));
        }))
      });
    },
  }
})
