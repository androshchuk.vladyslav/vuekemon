import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL } from "@/common/config";

export const Api = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },
  get(resource) {
    return Vue.axios.get(`${resource}/`);
  },
  getListed(resource, limit, offset = 1) {
    let promises = [];

    for (let i = offset; i < offset + limit; i++) {
      promises.push(Vue.axios.get(`${resource}/${i}/`));
    }

    return Vue.axios.all(promises).catch(error => {
      throw new Error(`Api ${error}`);
    });
  },
  getMany(resource, ids) {
    let promises = [];

    ids.forEach( id => {
      promises.push(Vue.axios.get(`${resource}/${id}/`));
    });

    return Vue.axios.all(promises).catch(error => {
      throw new Error(`Api ${error}`);
    });
  }
};

export default Api;