import Vue from 'vue'
import Router from 'vue-router'
import Pokemons from './views/Pokemons.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/1',
    },
    {
      path: '/:page(\\d+)',
      name: 'pokemons',
      component: Pokemons
    },
    {
      path: '/pokemon/:name',
      name: 'pokemonDetail',
      component: () => import('./views/PokemonDetail.vue')
    }
  ]
})
